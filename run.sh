#!/bin/bash

# 1 param - Allow "/list" url to see files list on server?
# 2 param - Set server port
# 3 param - Allow write logs to console?
# 4 param - Allow write logs to files in "/logs"?

sudo java -jar Servo-1.0.jar true 80 true true