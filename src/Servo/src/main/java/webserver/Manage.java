package webserver;

import javax.swing.*;

/**
 * Created by Denys Ordynskiy on 21.05.2017.
 */
public class Manage {
  public static void main(String[] args) {
    try {
      WebServer.allowList = args.length > 0 ? Boolean.parseBoolean(args[0]) : JOptionPane.showConfirmDialog(null, "Allow \"/list\" url to see files list on server?", "Init",
          JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
      WebServer webServer = new WebServer(args.length > 1 ? args[1] : JOptionPane.showInputDialog("Set server port"));
      WebServer.allowConsoleLog = args.length > 2 ? Boolean.parseBoolean(args[2]) : JOptionPane.showConfirmDialog(null, "Allow write logs to console?",
          "Init", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
      WebServer.allowFilesLog = args.length > 3 ? Boolean.parseBoolean(args[3]) : JOptionPane.showConfirmDialog(null, "Allow write logs to files in \"/logs\"?",
          "Init", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
      webServer.start();
      System.out.println("Server started.");
    } catch (Throwable throwable) {
      throwable.printStackTrace();
      System.out.println();
      System.err.println("Should be 4 parameters after *.jar !!!");
      System.err.println("1 param - Allow \"/list\" url to see files list on server?");
      System.err.println("2 param - Set server port");
      System.err.println("3 param - Allow write logs to console?");
      System.err.println("4 param - Allow write logs to files in \"/logs\"?");
    }
  }
}
