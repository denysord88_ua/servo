/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to you under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rss;

import java.util.Date;

public class NewsRecord {
  @Override
  public String toString() {
    return "" + date.getTime() + pageURL;
  }

  private String title;
  private String pageURL;
  private String pageHost;
  private String imageUrl;
  private String pageHTMLDescription;
  private Date date;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getPageURL() {
    return pageURL;
  }

  public void setPageURL(String pageURL) {
    this.pageURL = pageURL;
  }

  public String getPageHost() {
    return pageHost;
  }

  public void setPageHost(String pageHost) {
    this.pageHost = pageHost;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getPageHTMLDescription() {
    return pageHTMLDescription;
  }

  public void setPageHTMLDescription(String pageHTMLDescription) {
    this.pageHTMLDescription = pageHTMLDescription;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }
}
