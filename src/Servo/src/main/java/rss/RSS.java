/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to you under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rss;

import files.finder.FileProcessor;
import files.parser.FileParser;
import org.apache.commons.text.StringEscapeUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.Locale;

public class RSS {
  public static String getRSS() throws Exception {
    LinkedList<NewsRecord> newsRecords = new LinkedList<>();
    newsRecords.addAll(DB.loadAllNewsFromDB());
    sort(newsRecords);
    return createRSSPage(newsRecords);
  }

  public static String getRSS(LinkedList<String> hosts) throws Exception {
    LinkedList<NewsRecord> newsRecords = new LinkedList<>();
    newsRecords.addAll(DB.loadNewsByHostsFromDB(hosts));
    sort(newsRecords);
    return createRSSPage(newsRecords);
  }

  private static String createRSSPage(LinkedList<NewsRecord> newsRecords) throws Exception {
    StringBuffer sb = new StringBuffer();

    sb.append("<!doctype html>");
    sb.append("<html>");
    sb.append("<head>");
    sb.append("<title>RSS by Denys Ordynskiy</title>");
    sb.append("<script src=\"https://code.jquery.com/jquery-3.2.1.min.js\"></script>");
    sb.append("<script src=\"js/script.js\"></script>");
    sb.append("<link href=\"/css/style.css\" id=\"theme\" type=\"text/css\" rel=\"stylesheet\">");
    sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>");
    sb.append("<link rel=\"shortcut icon\" href=\"/img/favicon.ico\" type=\"image/x-icon\">");
    sb.append("</head>");
    sb.append("<body>");
    sb.append("<div class=\"container\">");
    sb.append("<form class=\"tabs\">");

    for(String host : DB.loadAllHostsFromDB()) {
      /*
      * <input type="checkbox" id="subscribeNews" name="subscribe" value="newsletter">
    <label for="subscribeNews">Subscribe to newsletter?</label>
      * */
      sb.append("<input type=\"checkbox\" class=\"tabElement\" id=\"" + host + "\" name=\"" + host + "\" value=\"" + host + "\">");
      sb.append("<label for=\"" + host + "\" class=\"tabElement\">" + host + "</label>");
      //sb.append("<li class=\"tabElement\" id=\"" + host + "\">");
      //sb.append("<a href=\"#" + host + "\" class=\"tabLink\">" + host + "</a>");
      //sb.append("</li>");
    }

    sb.append("<button type=\"submit\">Show selected channels</button>");
    sb.append("</form>");
    sb.append("<br />");
    sb.append("<div class=\"containerClass\" id=\"containerId\">");
    sb.append("<a href=\"list\">List of files</a> ");
    sb.append("<a href=\"/\">Home</a> ");
    sb.append("<a href=\"https://github.com/denysord88/Servo\">Source on Github</a>");
    sb.append("<br /><br />");
    sb.append("________________________________________<br />");

    for(NewsRecord nr : newsRecords) {
      sb.append("<br /><span id=\"title\">" + nr.getTitle() + "</span><br />");
      sb.append("<span id=\"host\">" + nr.getPageHost() + "</span><br />");
      sb.append("<a target=\"_blank\" href=\"" + nr.getPageURL() + "\" id=\"pageUrl\"><img height=\"150px\" src=\"" + nr.getImageUrl() + "\" id=\"image\" /></a><br />");
      sb.append("<span id=\"description\">" + nr.getPageHTMLDescription() + "</span><br /><br />");
      sb.append("<span id=\"date\">" + new SimpleDateFormat("HH:mm:ss dd/MM/yyyy").format(nr.getDate()) + "</span><br />");
      sb.append("________________________________________<br /><br />");
    }


    sb.append("</div>");
    sb.append("<br /><br />");
    sb.append("<button onclick=\"add('myDiv')\">add</button>");
    sb.append("<button onclick=\"send()\">send</button>");
    sb.append("</div>");
    sb.append("<br />");
    sb.append("<div class=\"copyrt\">© Denys Ordynskiy</div>");
    sb.append("</body>");
    sb.append("</html>");

    return sb.toString();
  }

  private static void updateRSS() throws Exception {
    DB.saveNewsToDB(getRadioTNews(), true);
    DB.saveNewsToDB(getUnianNews(), true);
    DB.saveNewsToDB(getThevergeNews(), true);
    DB.saveNewsToDB(getXakepNews(), true);
    DB.saveNewsToDB(getKorrespondentNews(), true);
  }

  private static void sort(LinkedList<NewsRecord> news) {
    NewsRecord one;
    NewsRecord two;
    for (int j = 0; j < news.size(); j++) {
      for (int i = 0; i < news.size() - 1; i++) {
        one = news.get(i);
        two = news.get(i + 1);
        if (one.getDate().getTime() < two.getDate().getTime()) {
          news.set(i, two);
          news.set(i + 1, one);
        }
      }
    }
  }

  private static LinkedList<NewsRecord> getRadioTNews() throws Exception {
    URL url = new URL("http://feeds.rucast.net/radio-t");
    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
    String inputLine;

    LinkedList<String> news = new LinkedList<>();
    StringBuilder sb = new StringBuilder();
    boolean head = true;
    while ((inputLine = in.readLine()) != null) {
      if (inputLine.trim().equals("<item>") || inputLine.trim().equals("<media:credit role=\"author\">")) {
        if (!head) news.add(sb.toString().trim());
        sb = new StringBuilder();
        head = false;
      }
      if (!head) sb.append(inputLine);
    }
    in.close();

    LinkedList<NewsRecord> newsRecords = new LinkedList<>();
    for (String n : news) {
      NewsRecord record = new NewsRecord();
      record.setTitle(FileParser.getTextByNearestBoundaries("<title>", "</title>", n).trim());
      record.setPageURL(FileParser.getTextByNearestBoundaries("<link>", "</link>", n).trim());
      record.setPageHost("radio-t.com");
      record.setImageUrl(FileParser.getTextByNearestBoundaries("<description><![CDATA[<p><img src=\"", "\"", n).trim());
      String pageDesc = "<ul>" + FileParser.getTextByNearestBoundaries("<ul>", "</ul>", n).trim() + "</ul>";
      pageDesc += "<p><a href=\"http://cdn.radio-t.com" + FileParser.getTextByNearestBoundaries("<p><a href=\"http://cdn.radio-t.com", "</audio>", n).trim() + "</audio>";
      record.setPageHTMLDescription(pageDesc.replace("<audio src=\"", "<audio controls><source src=\"").replace(" preload=\"none\"></audio>", "\" type=\"audio/mpeg\">Your browser does not support the audio element.</audio>"));
      record.setDate(new SimpleDateFormat("EEE, dd MMM yyyy kk:mm:ss z", Locale.ENGLISH).parse(FileParser.getTextByNearestBoundaries("<pubDate>", "</pubDate>", n)));
      newsRecords.add(record);
    }
    return newsRecords;
  }

  private static LinkedList<NewsRecord> getUnianNews() throws Exception {
    URL url = new URL("https://rss.unian.net/site/news_rus.rss");
    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
    String inputLine;

    LinkedList<String> news = new LinkedList<>();
    StringBuilder sb = new StringBuilder();
    boolean head = true;
    while ((inputLine = in.readLine()) != null) {
      if (inputLine.trim().equals("<item>") || inputLine.trim().equals("</channel>")) {
        if (!head) news.add(sb.toString().trim());
        sb = new StringBuilder();
        head = false;
      }
      if (!head) sb.append(inputLine);
    }
    in.close();

    LinkedList<NewsRecord> newsRecords = new LinkedList<>();
    for (String n : news) {
      NewsRecord record = new NewsRecord();
      record.setTitle(FileParser.getTextByNearestBoundaries("<title><![CDATA[", "]]></title>", n).trim());
      record.setPageURL(FileParser.getTextByNearestBoundaries("<link>", "</link>", n).trim());
      record.setPageHost("unian.net");
      record.setImageUrl(FileParser.getTextByNearestBoundaries("<enclosure url=\"", "\"", n).trim());
      record.setPageHTMLDescription(FileParser.getTextByNearestBoundaries("<description><![CDATA[", "]]></description>", n).trim());
      record.setDate(new SimpleDateFormat("EEE, dd MMM yyyy kk:mm:ss z", Locale.ENGLISH).parse(FileParser.getTextByNearestBoundaries("<pubDate>", "</pubDate>", n)));
      // <pubDate>Thu, 31 May 2018 12:07:43 +0000</pubDate>
      newsRecords.add(record);
    }
    return newsRecords;
  }

  private static LinkedList<NewsRecord> getThevergeNews() throws Exception {
    URL url = new URL("https://www.theverge.com/rss/index.xml");
    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
    String inputLine;

    LinkedList<String> news = new LinkedList<>();
    StringBuilder sb = new StringBuilder();
    StringBuilder sb2 = new StringBuilder();
    boolean head = true;
    while ((inputLine = in.readLine()) != null) {
      sb2.append(inputLine + "\r\n");
      if (inputLine.trim().equals("<entry>") || inputLine.trim().equals("</feed>")) {
        if (!head) news.add(sb.toString().trim());
        sb = new StringBuilder();
        head = false;
      }
      if (!head) sb.append(inputLine);
    }
    in.close();

    LinkedList<NewsRecord> newsRecords = new LinkedList<>();
    for (String n : news) {
      NewsRecord record = new NewsRecord();
      record.setTitle(FileParser.getTextByNearestBoundaries("<title>", "</title>", n).trim());
      record.setPageURL(FileParser.getTextByNearestBoundaries("<link rel=\"alternate\" type=\"text/html\" href=\"", "\"", n).trim());
      record.setPageHost("theverge.com");
      String imgUrl = FileParser.getTextByNearestBoundaries("img alt=\"", " /&gt;", n);
      imgUrl = FileParser.getTextByNearestBoundaries("src=\"", "\"", imgUrl);
      if(imgUrl == null) imgUrl = "https://fiu-assets-2-syitaetz61hl2sa.stackpathdns.com/static/use-media-items/16/15689/full-1000x924/56702b75/The_Verge_logo.png?resolution=0";
      record.setImageUrl(imgUrl.trim());
      record.setPageHTMLDescription(FileParser.getTextByNearestBoundaries("<content type=\"html\">", "ntent>", n).trim());
      record.setPageHTMLDescription(StringEscapeUtils.unescapeHtml4((FileParser.getTextByNearestBoundaries((record.getPageHTMLDescription().contains("/&gt;") ?"/&gt;":""), "</co", record.getPageHTMLDescription()).trim())));
      String time = FileParser.getTextByNearestBoundaries("<publis", "</published>", n);
      time = FileParser.getTextByOutsideBoundaries("hed>", "-", time);
      record.setDate(new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.ENGLISH).parse(time.replace('T', ' ')));
      // <pubDate>Thu, 31 May 2018 12:07:43 +0000</pubDate>
      newsRecords.add(record);
    }
    return newsRecords;
  }

  private static LinkedList<NewsRecord> getXakepNews() throws Exception {
    URL url = new URL("https://xakep.ru/feed/");
    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
    String inputLine;

    LinkedList<String> news = new LinkedList<>();
    StringBuilder sb = new StringBuilder();
    boolean head = true;
    while ((inputLine = in.readLine()) != null) {
      if (inputLine.trim().equals("<item>") || inputLine.trim().equals("</channel>")) {
        if (!head) news.add(sb.toString().trim());
        sb = new StringBuilder();
        head = false;
      }
      if (!head) sb.append(inputLine);
    }
    in.close();

    LinkedList<NewsRecord> newsRecords = new LinkedList<>();
    for (String n : news) {
      NewsRecord record = new NewsRecord();
      record.setTitle(FileParser.getTextByNearestBoundaries("<title>", "</title>", n).trim());
      record.setPageURL(FileParser.getTextByNearestBoundaries("<link>", "</link>", n).trim());
      record.setPageHost("xakep.ru");
      record.setImageUrl(FileParser.getTextByNearestBoundaries("<description><![CDATA[<img src=\"", "\"", n).trim());
      record.setPageHTMLDescription(FileParser.getTextByNearestBoundaries("<description><![CDATA[", "</description>", n).trim());
      record.setPageHTMLDescription(FileParser.getTextByNearestBoundaries(">", "]]>", record.getPageHTMLDescription()));
      record.setDate(new SimpleDateFormat("EEE, dd MMM yyyy kk:mm:ss z", Locale.ENGLISH).parse(FileParser.getTextByNearestBoundaries("<pubDate>", "</pubDate>", n)));
      // <pubDate>Thu, 31 May 2018 12:07:43 +0000</pubDate>
      newsRecords.add(record);
    }
    return newsRecords;
  }

  private static LinkedList<NewsRecord> getKorrespondentNews() throws Exception {
    URL url = new URL("http://k.img.com.ua/rss/ru/all_news2.0.xml");
    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
    String inputLine;

    LinkedList<String> news = new LinkedList<>();
    StringBuilder sb = new StringBuilder();
    boolean head = true;
    while ((inputLine = in.readLine()) != null) {
      if (inputLine.trim().equals("<item>") || inputLine.trim().equals("</channel>")) {
        if (!head) news.add(sb.toString().trim());
        sb = new StringBuilder();
        head = false;
      }
      if (!head) sb.append(inputLine);
    }
    in.close();

    LinkedList<NewsRecord> newsRecords = new LinkedList<>();
    for (String n : news) {
      NewsRecord record = new NewsRecord();
      record.setTitle(FileParser.getTextByNearestBoundaries("<title><![CDATA[", "]]></title>", n).trim());
      record.setPageURL(FileParser.getTextByNearestBoundaries("<link><![CDATA[", "]]></link>", n).trim());
      record.setPageHost("korrespondent.net");
      record.setImageUrl(FileParser.getTextByNearestBoundaries("<enclosure url=\"", "\"", n).trim());
      record.setPageHTMLDescription(FileParser.getTextByNearestBoundaries("<description><![CDATA[", "</description>", n).trim());
      record.setPageHTMLDescription(FileParser.getTextByNearestBoundaries(">", "]]>", record.getPageHTMLDescription()));
      record.setDate(new SimpleDateFormat("EEE, dd MMM yyyy kk:mm:ss z", Locale.ENGLISH).parse(FileParser.getTextByNearestBoundaries("<pubDate><![CDATA[", "]]></pubDate>", n)));
      // <pubDate>Thu, 31 May 2018 12:07:43 +0000</pubDate>
      newsRecords.add(record);
    }
    return newsRecords;
  }
}
