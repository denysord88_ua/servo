/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to you under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rss;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;

public class DB {
  private static final String DB_CONN_URL = "jdbc:sqlite:" + new File("").getAbsolutePath() + "/News.sqlite";
  private static final String TABLE_NAME = "news";

  public static void initDatabase(String fileName) {

    String url = "jdbc:sqlite:" + new File("").getAbsolutePath() + "/" + fileName;

    try (Connection conn = DriverManager.getConnection(url)) {
      if (conn != null) {
        System.out.println("Database is readable now.");
      }

    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }
  }

  private static void initTable() throws Exception {
    String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (\n"
        + "	title text NOT NULL,\n"
        + "	pageURL text NOT NULL,\n"
        + "	pageHost text NOT NULL,\n"
        + "	imageUrl text NOT NULL,\n"
        + "	pageHTMLDescription text NOT NULL,\n"
        + "	postDate int NOT NULL\n"
        + ");";

    Connection conn = DriverManager.getConnection(DB_CONN_URL);
    Statement stmt = conn.createStatement();
    stmt.execute(sql);
    System.out.println("Table ready.");
  }

  private static void dropOldNews() throws Exception {
    Connection conn = DriverManager.getConnection(DB_CONN_URL);
    String sql = "DELETE FROM " + TABLE_NAME + " WHERE postDate < ?";
    PreparedStatement pstmt = conn.prepareStatement(sql);
    // set the corresponding param
    Calendar cal = new GregorianCalendar();
    cal.setTime(new Date());
    // 30 days ago
    cal.add(Calendar.DAY_OF_MONTH, -30);
    Date d = cal.getTime();
    pstmt.setLong(1, d.getTime());
    // execute the delete statement
    System.out.println(pstmt.executeUpdate() + " - old news deleted");
  }

  private static Long getLastNewsDateByRSSHost(String RSSHost) throws Exception {
    Connection conn = DriverManager.getConnection(DB_CONN_URL);
    Statement stmt = conn.createStatement();
    String sql = "SELECT max(postDate) FROM " + TABLE_NAME + " WHERE pageHost = \"" + RSSHost + "\";";
    ResultSet rs = stmt.executeQuery(sql);
    long res = 0;
    while (rs.next()) {
      res = rs.getLong("max(postDate)");
    }
    return res;
  }

  public static void saveNewsToDB(LinkedList<NewsRecord> news, boolean singleHost) throws Exception {
    if(news == null || news.isEmpty()) return;
    initDatabase("News.sqlite");
    initTable();
    dropOldNews();

    String sql = "INSERT INTO " + TABLE_NAME + " (title,pageURL,pageHost,imageUrl,pageHTMLDescription,postDate) VALUES(?,?,?,?,?,?);";
    Connection conn = DriverManager.getConnection(DB_CONN_URL);
    long lastPostDate = 0;
    if(singleHost) lastPostDate = getLastNewsDateByRSSHost(news.get(0).getPageHost());
    int saved = 0;
    for(NewsRecord n : news) {
      if(!singleHost) lastPostDate = getLastNewsDateByRSSHost(n.getPageHost());
      if(n.getDate().getTime() <= lastPostDate) continue;
      PreparedStatement pstmt = conn.prepareStatement(sql);
      pstmt.setString(1, n.getTitle());
      pstmt.setString(2, n.getPageURL());
      pstmt.setString(3, n.getPageHost());
      pstmt.setString(4, n.getImageUrl());
      pstmt.setString(5, n.getPageHTMLDescription());
      pstmt.setLong(6, n.getDate().getTime());
      pstmt.executeUpdate();
      saved++;
    }
    System.out.println(saved + " from " + news.size() + " news saved to DB for host " + news.get(0).getPageHost());
  }

  public static LinkedList<NewsRecord> loadAllNewsFromDB() throws Exception {
    LinkedList<NewsRecord> news = new LinkedList<>();
    String sql = "SELECT title, pageURL, pageHost, imageUrl, pageHTMLDescription, postDate FROM " + TABLE_NAME + ";";
    Connection conn = DriverManager.getConnection(DB_CONN_URL);
    Statement stmt = conn.createStatement();
    ResultSet rs    = stmt.executeQuery(sql);
    while (rs.next()) {
      NewsRecord nr = new NewsRecord();
      nr.setTitle(rs.getString("title"));
      nr.setPageURL(rs.getString("pageURL"));
      nr.setPageHost(rs.getString("pageHost"));
      nr.setImageUrl(rs.getString("imageUrl"));
      nr.setPageHTMLDescription(rs.getString("pageHTMLDescription"));
      nr.setDate(new Date(rs.getLong("postDate")));
      news.add(nr);
    }
    System.out.println(news.size() + " news loaded from DB");
    return news;
  }

  public static LinkedList<NewsRecord> loadNewsByHostsFromDB(LinkedList<String> hosts) throws Exception {
    LinkedList<NewsRecord> news = new LinkedList<>();
    String sql = "SELECT title, pageURL, pageHost, imageUrl, pageHTMLDescription, postDate FROM " + TABLE_NAME + " WHERE pageHost IN (";
    for(String host: hosts) {
      sql += "\"" + host + "\",";
    }
    sql = sql.substring(0, sql.length() - 1);
    sql += ");";
    System.out.println("!!! " + sql);
    Connection conn = DriverManager.getConnection(DB_CONN_URL);
    Statement stmt = conn.createStatement();
    ResultSet rs    = stmt.executeQuery(sql);
    while (rs.next()) {
      NewsRecord nr = new NewsRecord();
      nr.setTitle(rs.getString("title"));
      nr.setPageURL(rs.getString("pageURL"));
      nr.setPageHost(rs.getString("pageHost"));
      nr.setImageUrl(rs.getString("imageUrl"));
      nr.setPageHTMLDescription(rs.getString("pageHTMLDescription"));
      nr.setDate(new Date(rs.getLong("postDate")));
      news.add(nr);
    }
    System.out.println(news.size() + " news loaded from DB");
    return news;
  }

  public static LinkedList<String> loadAllHostsFromDB() throws Exception {
    LinkedList<String> hosts = new LinkedList<>();
    String sql = "SELECT DISTINCT pageHost FROM " + TABLE_NAME + ";";
    Connection conn = DriverManager.getConnection(DB_CONN_URL);
    Statement stmt = conn.createStatement();
    ResultSet rs    = stmt.executeQuery(sql);
    while (rs.next())
      hosts.add(rs.getString("pageHost"));
    return hosts;
  }
}
